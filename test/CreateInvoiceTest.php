<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;
use WikiSuite\ILP\Providers\ILPInvoiceServerClient;
use WikiSuite\ILP\Invoice;
use WikiSuite\ILP\PointerTrait;

class ILPInvoiceServerClientTest extends TestCase
{
    use PointerTrait;

    public function testCreateInvoice()
    {
        $client = new ILPInvoiceServerClient('http://localhost:6000', 'test', false);
        $amount = "900";
        $assetCode = "XRP";
        $assetScale = "9";
        $response = $client->createInvoice($amount, $assetCode, $assetScale, "", ["reason" => "more stuff"]);
        $this->assertIsString($response);

        $invoice = $client->getInvoice($response);
        $this->assertInstanceOf(Invoice::class, $invoice);

        $this->assertEquals($invoice->getAmount(), $amount);
        $this->assertEquals($invoice->getAssetCode(), $assetCode);
        $this->assertEquals($invoice->getCodeScale(), $assetScale);
        $this->assertIsArray($invoice->getAdditionalFields());
        $this->assertNotEmpty($invoice->getAdditionalFields());
        $this->assertIsString($invoice->getDestinationAccount());
        $this->assertIsNumeric($invoice->getBalance());
        $this->assertIsString($invoice->getSharedSecret());
        $this->assertEquals(36, strlen($client->getPointerId($response)));
        $this->assertFalse($client->getPointerId("http://wrongurl.com"));
        $this->assertFalse($client->getPointerId("randomstring"));
        $this->assertFalse($invoice->isPayed());

        $this->helperMakePayment($invoice->getAmount(), $this->resolvePointer($response, false));

        $invoice = $client->getInvoice($response);
        $this->assertTrue($invoice->isPayed());
    }

    /**
     * Helper function to send payment to a give pointer
     *
     * @param int $amount
     * @param string $pointer
     * @return bool
     */
    protected function helperMakePayment(int $amount, string $pointer)
    {
        $process = new Process(["ilp-spsp", "send", "-a", $amount, "-p", $pointer]);
        return ($process->run() === 0);
    }
}
