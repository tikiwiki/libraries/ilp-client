FROM alpine:latest

WORKDIR /app
RUN apk add nodejs-current npm git curl openrc
RUN git clone https://github.com/interledgerjs/ilp-spsp-invoice-server.git .
RUN npm install -g pm2@latest moneyd
RUN npm install
ENTRYPOINT pm2 start 'moneyd local' && node index.js

